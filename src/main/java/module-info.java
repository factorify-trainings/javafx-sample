module sample.main {
    requires java.net.http;
    requires javafx.controls;
    requires javafx.fxml;

    exports me.factorify;
}
